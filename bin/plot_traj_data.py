#!/usr/bin/env python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import argparse
import os

import numpy as np
import matplotlib.pyplot as plt

ap = argparse.ArgumentParser()
ap.add_argument('folder', nargs='?', default='.')
ap.add_argument('--no_fci', action='store_true')
ap.add_argument('--no_pos', action='store_true')
ap.add_argument('--no_vel', action='store_true')
ap.add_argument('--no_acc', action='store_true')
ap.add_argument('--no_show', action='store_true')
ap.add_argument('--t_range', type=str, default='(0.0, 5.0)')
ap.add_argument('--file', type=str, default='')
args = ap.parse_args()

n_plots = 3
if args.no_pos:
    n_plots -= 1
if args.no_vel:
    n_plots -= 1
if args.no_acc:
    n_plots -= 1

args.t_range = eval(args.t_range)

print('Plotting data from folder "{}"'.format(args.folder))

cli_path = os.path.join(args.folder, 'cli_traj.npy')
fci_cmd_path = os.path.join(args.folder, 'fci_cmd_traj.npy')
fci_act_path = os.path.join(args.folder, 'fci_act_traj.npy')

cli_traj = np.fromfile(cli_path, dtype='4d')
cli_mask = (cli_traj[:, 0] >= args.t_range[0]) & (cli_traj[:, 0] <= args.t_range[1])
cli_traj = cli_traj[cli_mask]
#time_range = np.array(args.t_range)
#clidx_range = (100 * (time_range-t0)).astype(np.uint32)
#clidx = slice(*clidx_range)
# Reset cli_traj to selected range
#cli_traj = cli_traj[clidx]
#fcidx_range = 10 * clidx_range
#fcidx = slice(*fcidx_range)
cli_t, cli_p, cli_v, cli_a = cli_traj.T
#cli_t = 0.01 * np.arange(cli_t.size)
cli_dt =np.diff(cli_t)
cli_vt = cli_t[:-1] + 0.5 * cli_dt
cli_pv = np.diff(cli_p) / cli_dt


# fci_cmd contains the commanded positions to the controller
fci_cmd_traj = np.fromfile(fci_cmd_path, dtype='2d')
fci_cmd_mask =(fci_cmd_traj[:, 0] >= args.t_range[0]) & (fci_cmd_traj[:, 0] <= args.t_range[1])
fci_cmd_traj = fci_cmd_traj[fci_cmd_mask]
fci_cmd_t, fci_cmd_p = fci_cmd_traj.T[:2]
#fci_cmd_t = 0.001 * np.arange(fci_cmd_t.size)
fci_cmd_dt = np.diff(fci_cmd_t)
fci_cmd_vt = fci_cmd_t[:-1] + 0.5 * fci_cmd_dt
fci_cmd_v = np.diff(fci_cmd_p) / fci_cmd_dt
fci_cmd_at = fci_cmd_t[1:-1]
fci_cmd_a = np.diff(fci_cmd_v) / fci_cmd_dt[:-1]

# fci_act contains the actual positions from the controller
fci_act_traj = np.fromfile(fci_act_path, dtype='2d')
fci_act_mask =(fci_act_traj[:, 0] >= args.t_range[0]) & (fci_act_traj[:, 0] <= args.t_range[1])
fci_act_traj = fci_act_traj[fci_act_mask]
fci_act_t, fci_act_p = fci_act_traj.T[:2]
#fci_act_t = 0.001 * np.arange(fci_act_t.size)
fci_act_dt = np.diff(fci_act_t)
fci_act_vt = fci_act_t[:-1] + 0.5 * fci_act_dt
fci_act_v = np.diff(fci_act_p) / fci_act_dt
fci_act_at = fci_act_t[1:-1]
fci_act_a = np.diff(fci_act_v) / fci_act_dt[:-1]

cli_style = dict(color='blue', label='MA', markersize = 4, marker = 'o')
# fci_style = dict(color='red', label='FCI-commanded', markersize=4, marker = 'x')
fci_cmd_style = dict(color='green', label='FMS', markersize=4, marker = 'o', linewidth=1)
fci_act_style = dict(color='red', label='FCI', markersize=2, marker = 'x', linewidth=0.5)
#plt.plot(*(traj.T[0:2]), marker='o')
plot_no = 1
fig = plt.figure()
fig.set_size_inches((7,0.5+2*n_plots))
fig.suptitle('Trajectories from folder "{}"'.format(args.folder))
if not args.no_pos:
    ax_p = plt.subplot(n_plots, 1, plot_no)
    ax_p.set_title('Joint Positions')
    ax_p.plot(cli_t, cli_p, **cli_style)
    ax_p.plot(fci_cmd_t, fci_cmd_p, **fci_cmd_style)
    if not args.no_fci:
        ax_p.plot(fci_act_t, fci_act_p, **fci_act_style)
    ax_p.legend()
    ax_p.grid()
    ax_p.set_ylabel(r'$p\quad [rad]$')
    ax_p.set_xlabel('$t\quad [s]$')
    plot_no +=1 
if not args.no_vel:
    ax_v = plt.subplot(n_plots, 1, plot_no)
    ax_v.set_title('Joint Velocities')
    ax_v.plot(cli_t, cli_v, **cli_style)
    ax_v.plot(fci_cmd_vt, fci_cmd_v, **fci_cmd_style)
    if not args.no_fci:
        ax_v.plot(fci_act_vt, fci_act_v, **fci_act_style)
    ax_v.legend()
    ax_v.grid()
    ax_v.set_ylabel(r'$v\quad [\frac{{rad}}{s}]$')
    ax_v.set_xlabel('$t\quad [s]$')
    plot_no +=1 
if not args.no_acc:
    ax_a = plt.subplot(n_plots, 1, plot_no)
    ax_a.set_title('Joint Accelerations')
    ax_a.plot(cli_t, cli_a, **cli_style)
    ax_a.plot(fci_cmd_at, fci_cmd_a, **fci_cmd_style)
    if not args.no_fci:
        ax_a.plot(fci_act_at, fci_act_a, **fci_act_style)
    ax_a.legend()
    ax_a.grid()
    ax_a.set_ylabel(r'$a\quad [\frac{{rad}}{s^2}]$')
    ax_a.set_xlabel('$t\quad [s]$')
    plot_no +=1 

plt.tight_layout(rect=[0, 0, 1, 0.95])

if args.file != '':
    plt.savefig(args.file)

if not args.no_show:
    plt.show(block=False)
