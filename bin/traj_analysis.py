# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import numpy as np
import matplotlib.pyplot as plt

p = np.fromfile('traj.bin', dtype=np.double)
p -= p[0]
t = 0.001 * np.arange(p.size)
v = np.diff(p) / 0.001
a = np.diff(v) / 0.001
j = np.diff(a) / 0.001

order = 3

traj = np.vstack((t[order:], p[order:], v[order-1:], a[order-2:], j)).T

plt.subplot(411)
plt.plot(*traj[:,[0,1]].T,color='red', label='pos', marker='x')
plt.legend()

plt.subplot(412)
plt.plot(*traj[:,[0,2]].T,color='green', label='vel', marker='x')
plt.legend()

plt.subplot(413)
plt.plot(*traj[:,[0,3]].T,color='blue', label='acc', marker='x')
plt.legend()

plt.subplot(414)
plt.plot(*traj[:,[0,4]].T,color='black', label='jrk', marker='x')
plt.legend()
plt.show(block=False)
