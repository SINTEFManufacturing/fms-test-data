#!/usr/bin/env python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import argparse
import os

import numpy as np
import matplotlib.pyplot as plt
import math3d as m3d


ap = argparse.ArgumentParser()
ap.add_argument('folder', nargs='?', default='.')
ap.add_argument('--no_F', action='store_true')
ap.add_argument('--no_K', action='store_true')
ap.add_argument('--no_cs', action='store_true')
ap.add_argument('--no_traj', action='store_true')
ap.add_argument('--no_show', action='store_true')
ap.add_argument('--t_range', type=str, default='(-0.25, 20.0)')
ap.add_argument('--file', type=str, default='')
args = ap.parse_args()

print('Plotting data from folder "{}"'.format(args.folder))

n_plots = 4
if args.no_K:
    n_plots -= 1
if args.no_F:
    n_plots -= 1
if args.no_cs:
    n_plots -= 1
if args.no_traj:
    n_plots -= 1

args.t_range = eval(args.t_range)

if not args.no_traj:
    traj = np.load(os.path.join(args.folder, 'cutting_traj.npy'))
    # Make times start at zero
    traj_t0 = traj[0, 0]
    traj[:, 0] -= traj_t0
    # Mask out to desired range
    traj_mask = (traj[:, 0] >= args.t_range[0]) & (traj[:, 0] <= args.t_range[1])
    traj = traj[traj_mask]
    traj_t = traj[:, 0]
    Bps = traj[:, 1:]

tft = np.load(os.path.join(args.folder, 't_ft.npy'))
# Make times start at zero
if not args.no_traj:
    tft[:, 0] -= traj_t0
else:
    tft[:, 0] -= tft[0, 0]
tft_mask = (tft[:, 0] >= args.t_range[0]) & (tft[:, 0] <= args.t_range[1])
tft = tft[tft_mask]
ft_t = tft[:,0]
Kfs = tft[:,1:4]  # Forces in stiffness frame
Kts = tft[:,4:]  # Torques in stiffness frame


if not args.no_traj:
    time_view_range = (min(ft_t[0], traj_t[0]), max(ft_t[-1], traj_t[-1]))
else:
    time_view_range = (ft_t[0], ft_t[-1])

FK = m3d.Orientation.new_rot_z(0).array
Ffs = FK.dot(Kfs.T).T
Fts = FK.dot(Kts.T).T

BF = m3d.Transform(np.load(os.path.join(args.folder, 'tool_pose_0.npy')))
FB = BF.inverse
Bcut_dir = m3d.Vector(np.load(os.path.join(args.folder, 'cut_dir.npy')))
Bshear_dir = m3d.Vector(np.load(os.path.join(args.folder, 'shear_dir.npy')))
Fcut_dir = FB.orient * Bcut_dir
Fshear_dir = FB.orient * Bshear_dir
Ff_cuts = Fcut_dir.array.dot(Ffs.T)
Ff_shears = Fshear_dir.array.dot(Ffs.T)

f_style=dict(marker='x', markersize=2, linewidth=1)
cli_style = dict(color='blue', label='Client', markersize = 4, marker = 'o')
# fci_style = dict(color='red', label='FCI-commanded', markersize=4, marker = 'x')
fci_cmd_style = dict(color='green', label='FCI-commanded', markersize=4, marker = 'o', linewidth=1)
fci_act_style = dict(color='red', label='FCI-actual', markersize=2, marker = 'x', linewidth=0.5)
#plt.plot(*(traj.T[0:2]), marker='o')
plot_no = 1
fig = plt.figure()
fig.set_size_inches((7,0.5+2*n_plots))
fig.suptitle('Cutting data from folder "{}"'.format(args.folder))
if not args.no_K:
    ax_K=plt.subplot(n_plots, 1, plot_no)
    ax_K.set_title('Forces in K')
    ax_K.plot(ft_t, Kfs[:,0], label='$^{\cal K}f_x$', **f_style)
    ax_K.plot(ft_t, Kfs[:,1], label='$^{\cal K}f_y$', **f_style)
    ax_K.plot(ft_t, Kfs[:,2], label='$^{\cal K}f_z$', **f_style)
    ax_K.set_xlabel('$t\quad[s]$')
    ax_K.set_ylabel('$F\quad[N]$')
    ax_K.axhline(color='red')
    ax_K.set_xlim(*time_view_range)
    ax_K.legend()
    ax_K.grid()
    plot_no += 1
if not args.no_F:
    ax_F=plt.subplot(n_plots, 1, plot_no)
    ax_F.set_title('Forces in F')
    ax_F.plot(ft_t, Ffs[:,0], label='$^{\cal F}f_x$', **f_style)
    ax_F.plot(ft_t, Ffs[:,1], label='$^{\cal F}f_y$', **f_style)
    ax_F.set_xlabel('$t\quad[s]$')
    ax_F.set_ylabel('$F\quad[N]$')
    ax_F.axhline(color='red')
    ax_F.set_xlim(*time_view_range)
    ax_F.legend()
    ax_F.grid()
    plot_no += 1
if not args.no_cs:
    ax_cs = plt.subplot(n_plots, 1, plot_no)
    ax_cs.set_title('Forces')
    ax_cs.plot(ft_t, Ff_cuts, label='$\mathbf{f}\cdot \hat{\mathbf{c}}_c$', **f_style)
    ax_cs.plot(ft_t, Ff_shears, label='$\mathbf{f}\cdot \hat{\mathbf{s}}_c$', **f_style)
    ax_cs.set_xlabel('$t\quad[s]$')
    ax_cs.set_ylabel('$F\quad[N]$')
    ax_cs.set_xlim(*time_view_range)
    ax_cs.axhline(color='red')
    ax_cs.legend()
    ax_cs.grid()
    plot_no += 1
if not args.no_traj:
    Fps = FB.orient.array.dot(Bps.T).T + FB.pos.array
    ax_t = plt.subplot(n_plots, 1, plot_no)
    ax_t.set_title('Position trajectory')
    ax_t.plot(traj_t, Fps.dot(Fcut_dir.array), label='$\Delta\mathbf{p}\cdot\hat{\mathbf{c}}_c$', **f_style)
    ax_t.plot(traj_t, Fps.dot(Fshear_dir.array), label='$\Delta\mathbf{p}\cdot\hat{\mathbf{s}}_c$', **f_style)
    ax_t.set_xlabel('$t\quad[s]$')
    ax_t.set_ylabel('$p\quad[m]$')
    ax_t.set_xlim(*time_view_range)
    ax_t.legend()
    ax_t.grid()
    plot_no += 1

plt.tight_layout(rect=[0, 0, 1, 0.95])

if args.file != '':
    plt.savefig(args.file)

if not args.no_show:
    plt.show(block=False)
